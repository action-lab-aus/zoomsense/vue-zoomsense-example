import Vue from "vue";
import App from "./App.vue";
import ZoomSense from "@zoomsense/vue-zoomsense";
import { rtdbPlugin } from "vuefire";

Vue.config.productionTip = false;

let config = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  appId: process.env.VUE_APP_APP_ID,
};

Vue.use(ZoomSense, config);

Vue.use(rtdbPlugin);

new Vue({
  render: (h) => h(App),
}).$mount("#app");

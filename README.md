# vue-zoomsense-example

This is a simple example of how to implement a 'data consumption' augmentation (live read-only integration) with a ZoomSense deployment using just an anonymous access share token, and the `vue-zoomsense` npm library at https://gitlab.com/action-lab-aus/zoomsense/vue-zoomsense.

## ZoomSense Setup

Create a file called `.env` with the following variables which point to a valid ZoomSense deployment:

```
VUE_APP_API_KEY=
VUE_APP_AUTH_DOMAIN=
VUE_APP_DATABASE_URL=
VUE_APP_PROJECT_ID=
VUE_APP_FUNCTION_URL=
VUE_APP_APP_ID=
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
